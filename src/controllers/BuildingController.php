<?php

namespace app\controllers;

use yii\filters\auth\HttpBearerAuth;

class BuildingController extends \yii\rest\ActiveController
{
	public $modelClass = 'app\models\Building';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['class'] = HttpBearerAuth::className();
		$behaviors['authenticator']['except'] = ['index', 'view'];
		return $behaviors;
	}

}
