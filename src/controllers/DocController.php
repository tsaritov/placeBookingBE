<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * @SWG\Swagger(
 *     basePath="/",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(version="1.0", title="Simple API"),
 * )
 */
class DocController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

	/**
	 * @inheritdoc
	 */
	public function actions(): array
	{
		return [
			'docs' => [
				'class' => 'yii2mod\swagger\SwaggerUIRenderer',
				'restUrl' => Url::to(['doc/json-schema']),
			],
			'json-schema' => [
				'class' => 'yii2mod\swagger\OpenAPIRenderer',
				// Тhe list of directories that contains the swagger annotations.
				'scanDir' => [
					Yii::getAlias('@app/modules'),
					Yii::getAlias('@app/models'),
				],
			],
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

}
