<?php

namespace app\controllers;

use app\models\User;
use function MongoDB\BSON\toJSON;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex()
	{
		return $this->render('index');
	}

	/**
	 * @SWG\Post(path="/login",
	 *     tags={"Auth"},
	 *     summary="Retrieves the collection of User resources.",
	 *     @SWG\Parameter(
	 *            name="login",
	 *            in="body",
	 *            required=true,
	 * 			@SWG\Schema(ref="#/definitions/UserLogin"),
	 *        ),
	 *     @SWG\Response(
	 *         response = 200,
	 *         description = "Get auth barrer token",
	 *
	 *     ),
	 * )
	 */
	public function actionLogin()
	{
		$req = \GuzzleHttp\json_decode(Yii::$app->request->getRawBody());

		$user = User::findOne([
			"login"=>$req->login,
			"password"=>$req->password,
		]);

		if(is_null($user)){
			return null;
		}

		$user->generateAuthKey();
		$user->save();

		return \GuzzleHttp\json_encode(["tocken" => $user->access_token]);
	}

}
