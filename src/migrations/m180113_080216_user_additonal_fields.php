<?php

use yii\db\Migration;

/**
 * Class m180113_080216_user_additonal_fields
 */
class m180113_080216_user_additonal_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
    	$this->addColumn('user', 'email',$this->string(255));
    	$this->addColumn('user', 'phone',$this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
	    $this->dropColumn('user', 'email');
	    $this->dropColumn('user', 'phone');
    }


}
