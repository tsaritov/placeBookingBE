<?php

use yii\db\Migration;

/**
 * Class m180105_070809_user
 */
class m180105_070809_user extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function safeUp()
	{

		$this->createTable('user', [
			"id"      => $this->primaryKey(10)
			                  ->unsigned(),
			"login"   => $this->string(255)
			                  ->notNull(),
			"password" => $this->string(255)
			                  ->notNull(),
			"auth_key"   => $this->string(255),
		]);

	}

	/**
	 * @inheritdoc
	 */
	public function safeDown()
	{
		$this->dropTable('user');
	}
}
