<?php

use yii\db\Migration;

/**
 * Class m180115_062042_password_restore
 */
class m180115_062042_password_restore extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function safeUp()
	{
		$this->createTable('restore_password', [
			"id"            => $this->primaryKey(10)
			                        ->unsigned(),
			"user_id"       => $this->integer(10)
			                        ->notNull(),
			"token"         => $this->string(255)
			                        ->notNull(),
			"created_at"    => $this->dateTime(),
			"updated_at_at" => $this->dateTime(),

		]);

	}

	/**
	 * @inheritdoc
	 */
	public function safeDown()
	{
		$this->dropTable('restore_password');
	}

}
