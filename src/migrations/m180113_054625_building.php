<?php

use yii\db\Migration;

/**
 * Class m180113_054625_building
 */
class m180113_054625_building extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function safeUp()
	{
		$this->createTable('building', [
			"id"      => $this->primaryKey(10)
			                  ->unsigned(),
			"title"   => $this->string(255)
			                  ->notNull(),
			"owner"   => $this->integer(10)
			                  ->unsigned()
			                  ->notNull(),
			"address" => $this->string(255),
			"map"     => $this->string(255),
		]);

	}

	/**
	 * @inheritdoc
	 */
	public function safeDown()
	{
		$this->dropTable('building');
	}

}
