<?php

use yii\db\Migration;
use \app\models\Building;

/**
 * Class m180207_050042_buildings_image_field
 */
class m180207_050042_buildings_image_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    	$this->addColumn(Building::tableName(), 'images', $this->text());

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
	    $this->dropColumn(Building::tableName(), 'images');
    }

}
