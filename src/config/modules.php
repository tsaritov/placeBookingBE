<?php

return array_merge(require(__DIR__ . '/installed_modules.php'), [
	'core'  => ['class' => 'nullref\core\Module'],
	//'admin' => ['class' => 'nullref\admin\Module'],
	'admin' => [
		'class' => 'app\modules\admin\Admin',
	],
	'api'   => [
		'class'   => 'app\modules\api\api',
		'modules' => [
			'v1' => [
				'class' => 'app\modules\api\modules\v1\v1',
			],
		],
	],

	'gii' => [
		'class'      => 'yii\gii\Module',
		'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'] // adjust this to your needs
	],
]);