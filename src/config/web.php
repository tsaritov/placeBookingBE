<?php

$params = require(__DIR__ . '/params.php');
$modules = require(__DIR__ . '/modules.php');

$config = [
	'id'          => 'app',
	'name'        => 'Application',
	'basePath'    => dirname(__DIR__),
	'vendorPath'  => dirname(dirname(__DIR__)) . '/vendor',
	'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
	'bootstrap'   => ['log'],
	'components'  => [
		'services'     => [
			'class' => 'app\modules\api\modules\v1\components\Services',
		],
		's3'           => [
			'class'         => 'frostealth\yii2\aws\s3\Service',
			'credentials'   => [
				// Aws\Credentials\CredentialsInterface|array|callable
				'key'    => 'onctvIMfPA31pTPvp3sx70tneJADbLuEp/aNm9LT',
				'secret' => 'AKIAIVXF26QHE2DZIPPQ',
			],
			'region'        => 'eu-central',
			'defaultBucket' => 'lookatthat',
			'defaultAcl'    => 'public-read',
		],
		'formatter'    => [
			'class' => 'app\components\Formatter',
		],
		'request'      => [
			'enableCsrfValidation' => false,
			'cookieValidationKey'  => 'RiAveGUdUACvWZppHVevMJRGd5Rij8uh',
			'parsers'              => [
				'application/json' => 'yii\web\JsonParser',
			],
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
		'user'         => [
			'identityClass'   => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'authManager'  => [
			'class' => 'yii\rbac\DbManager',
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mailer'       => [
			'class'             => 'yii\swiftmailer\Mailer',
			'useFileTransport'  => YII_ENV_DEV,
			"fileTransportPath" => "/tmp/yiimails"
			// 'transport' => [
			// 	'class' => 'Swift_SmtpTransport',
			// 	'host' => 'smtp.gmail.com',
			// 	'username' => 'qweqw',
			// 	'password' => 'qwe',
			// 	'port' => '587',
			// 	'encryption' => 'tls',
			// ],

		],
		'i18n'         => [
			'translations' => [
				'*' => ['class' => 'yii\i18n\PhpMessageSource'],
			],
		],
		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'urlManager'   => [
			'enablePrettyUrl' => true,
			//'enableStrictParsing' => true,
			'showScriptName'  => false,
			'rules'           => require("rules.php"),
		],
		'db'           => require(__DIR__ . '/db.php'),
	],
	'modules'     => $modules,
	'params'      => $params,
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = 'yii\debug\Module';

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
