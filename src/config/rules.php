<?php
return [
	'<controller:\w+>/<id:\d+>' => '<controller>/view',
	'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
	'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
	'POST,GET login' => 'site/login',
	'GET profile' => 'site/profile',
	'POST /api/v1/building/imageadd/<id:\d>' => '/api/v1/building/imageadd',
	'POST /api/v1/building/imagedel/<id:\d>' => '/api/v1/building/imagedel',
	['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user'],
	['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/building'],
];