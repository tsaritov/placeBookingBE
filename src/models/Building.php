<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "building".
 *
 * @property int    $id
 * @property string $title
 * @property int    $owner
 * @property string $address
 * @property string $map
 * @property string $images
 */
class Building extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'building';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'address', 'map'], 'required'],
			[['owner'], 'integer'],
			[['images'], 'string'],
			[['title', 'address', 'map'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'      => 'ID',
			'title'   => 'Title',
			'owner'   => 'Owner',
			'address' => 'Address',
			'map'     => 'Map',
			'images'  => 'Images',
		];
	}

	// public function beforeSave($insert)
	// {
	//    if($this->isNewRecord){
	//    	$this->owner = Yii::$app->user->getId();
	//    }
	// }

	public function beforeSave($insert)
	{
		if ($this->isNewRecord) {
			$this->owner = Yii::$app->user->getId();
		}

		return parent::beforeSave($insert); // TODO: Change the autogenerated stub
	}

	public static function addImage($id, $image): self
	{
		$model = static::findOne(['id' => $id]);
		$images = json_decode($model->images);
		$images[] = $image;
		$model->images = json_encode($images);
		$model->save();

		return $model;

	}

	public static function delImage($id, $url)
	{
		$model = static::findOne(['id' => $id]);
		$model->images;
		$img_list = (array)\GuzzleHttp\json_decode($model->images);
		$img_list = array_filter($img_list, function ($e) use ($url) {
			return $e != $url;
		});
		$model->images = \GuzzleHttp\json_encode($img_list);
		$model->save();

		return $model;
	}

}
