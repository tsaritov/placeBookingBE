<?php

namespace app\modules\api\modules\v1\components\traits;

use yii\web\BadRequestHttpException;

trait FormTrait
{
	/**
	 * Returns an instance of the $class with loaded attributes and scenario.
	 *
	 * @param array  $attributes Attributes to be loaded to the form.
	 * @param string $scenario   Scenario to be loaded to the form.
	 * @param array  $config     Configuration data, which will be passed to the form constructor
	 *
	 * @return static
	 */
	public static function make(array $attributes, string $scenario = null, array $config = []): self
	{
		$model = new static($config);
		$model->setScenario(!is_null($scenario) ? $scenario : static::SCENARIO_DEFAULT);
		if (!$model->load($attributes)) {
			$model->setAttributes($attributes);
		}

		return $model;
	}

	public function validateOrException()
	{

		if (!$this->validate()) {
			$message =  json_encode( $this->getErrors());
			throw new BadRequestHttpException($message);
		}

	}

}