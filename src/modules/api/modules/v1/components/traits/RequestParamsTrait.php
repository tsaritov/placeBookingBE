<?php

namespace app\modules\api\modules\v1\components\traits;

use Yii;

trait RequestParamsTrait
{
	public function getRequestParams():array
	{
		$requestParams = Yii::$app->getRequest()
		                          ->getBodyParams();
		if (empty($requestParams)) {
			$requestParams = Yii::$app->getRequest()
			                          ->getQueryParams();

		}

		return $requestParams;

	}
}