<?php
namespace app\modules\api\modules\v1\components;
use app\modules\api\modules\v1\components\services\filemanager\FileManagerService;
use app\modules\api\modules\v1\components\services\pwd_restore\PasswordRestoreService;

/**
 * Class ServiceRouter
 * @package app\modules\api\modules\v1\components
 *
 * @property PasswordRestoreService $pwdRestore
 * @property FileManagerService $filemanager
 */
class Services
{
	private $map = [
		"pwdRestore"=> PasswordRestoreService::class,
		"filemanager"=> FileManagerService::class
	];

	private $services=[];

	public function __get($name)
	{
		if(!empty($this->services[$name])){
			return $this->services[$name];
		}

		if(empty($this->map[$name])){
			throw new \yii\base\Exception("There isn`t service $name");
		}

		$this->services[$name] = new $this->map[$name];

		return $this->services[$name];
	}

}