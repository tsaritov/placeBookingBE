<?php
namespace app\modules\api\modules\v1\components\services\filemanager;

use app\modules\api\modules\v1\components\services\filemanager\helpers\AWS;

class FileManagerService
{
	/**
	 * @var AWS
	 */
	protected $store;

	public function __construct()
	{
		$this->store = new AWS();
	}

	/**
	 * @param string $folder
	 * @param array  $tmp_array
	 *
	 * @return string
	 */
	public function saveTmpImage(string $folder, array $tmp_array):string
	{
		$res = $this->store->put($folder.'/'.$tmp_array['name'], fopen($tmp_array['tmp_name'], 'r'), $tmp_array['type'] );

		return $res['ObjectURL'];

	}

	public function deleteByUrl($url){


		return $this->store->del($url);
	}






}