<?php

namespace app\modules\api\modules\v1\components\services\pwd_restore;

use app\models\User;
use app\modules\api\modules\v1\components\services\pwd_restore\models\RestorePasswordModel;
use Yii;

class PasswordRestoreService
{

	public function create(string $email): RestorePasswordModel
	{

		$model = RestorePasswordModel::add($email);
		Yii::$app->mailer->compose()
		                 ->setFrom('from@domain.com')
		                 ->setTo($email)
		                 ->setSubject('Message subject')
		                 ->setHtmlBody('<b>Code: ' . $model->token . '</b>')
		                 ->send();

		return $model;
	}

	public function reset(string $token, string $password): void
	{

		$model = RestorePasswordModel::findOne(['token' => $token]);
		$user = User::findOne(['id' => $model->user_id]);
		$user->password = $password;
		$user->save();
		$model->delete();

	}

}