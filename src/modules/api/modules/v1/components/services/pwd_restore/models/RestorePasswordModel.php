<?php

namespace app\modules\api\modules\v1\components\services\pwd_restore\models;

use app\models\User;
use Yii;

/**
 * This is the model class for table "restore_password".
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property string $created_at
 * @property string $updated_at_at
 */
class RestorePasswordModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restore_password';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'token'], 'required'],
            [['user_id'], 'integer'],
            [['created_at', 'updated_at_at'], 'safe'],
            [['token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'token' => 'Token',
            'created_at' => 'Created At',
            'updated_at_at' => 'Updated At At',
        ];
    }

    public static function add($email):self
    {
    	$user = User::findOne(['login'=>$email]);
    	$model = new static();
    	$model->user_id = $user->id;
	    $model->token= md5('asdasd');
    	if(!$model->save()){

    		print_r( $model->getErrors() );
    		die();
	    }
    	return $model;
    }


}
