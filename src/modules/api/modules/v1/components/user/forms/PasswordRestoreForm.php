<?php

namespace app\modules\api\modules\v1\components\user\forms;

use app\models\User;
use app\modules\api\modules\v1\components\traits\FormTrait;
use yii\base\Model;

/**
 * Class PasswordRestoreForm
 * @package app\modules\api\modules\v1\components\user\forms
 *
 * @property string $email
 */
class PasswordRestoreForm extends Model
{
	use FormTrait;

	const SCENARIO_DEFAULT = 'default';
	/**
	 * @var string Email
	 */
	public $email;

	public function rules()
	{

		return [

				[['email'], 'required'],
				[['email'], 'email'],
				[
					'email',
					'exist',
					'targetClass'     => User::className(),
					'targetAttribute' => ['email'=>'login'],
					'message' => 'Email not found.'

				],


		];
	}

}