<?php

namespace app\modules\api\modules\v1\components\user\forms;


use app\modules\api\modules\v1\components\services\pwd_restore\models\RestorePasswordModel;
use app\modules\api\modules\v1\components\traits\FormTrait;
use yii\base\Model;

/**
 * Class ApiResponseForm
 * @package app\modules\api\modules\v1\components\user\forms
 *
 * @property string $login
 * @property string $phone
 */
class ApiResponseForm extends Model
{
	use FormTrait;

	const SCENARIO_DEFAULT = 'default';
	/**
	 * @var string Email
	 */
	public $login;
	public $phone;

	public function rules()
	{

		return [

				[['login'], 'required'],
				[['login', 'phone'], 'string']
		];
	}

}