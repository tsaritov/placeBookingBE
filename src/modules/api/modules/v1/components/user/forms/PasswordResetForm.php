<?php

namespace app\modules\api\modules\v1\components\user\forms;


use app\modules\api\modules\v1\components\services\pwd_restore\models\RestorePasswordModel;
use app\modules\api\modules\v1\components\traits\FormTrait;
use yii\base\Model;

/**
 * Class PasswordRestoreForm
 * @package app\modules\api\modules\v1\components\user\forms
 *
 * @property string $password
 * @property string $token
 */
class PasswordResetForm extends Model
{
	use FormTrait;

	const SCENARIO_DEFAULT = 'default';
	/**
	 * @var string Email
	 */
	public $password;
	public $token;

	public function rules()
	{

		return [

				[['token','password'], 'required'],
				[['token'], 'string'],
				[['password'], 'string'],
				[
					'token',
					'exist',
					'targetClass'     => RestorePasswordModel::className(),
					'targetAttribute' => ['token'=>'token'],
					'message' => 'Code not found.'

				],


		];
	}

}