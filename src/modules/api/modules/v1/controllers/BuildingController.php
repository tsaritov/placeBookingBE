<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\Building;
use app\modules\api\modules\v1\components\traits\RequestParamsTrait;
use yii\filters\auth\HttpBearerAuth;
use yii\web\ForbiddenHttpException;
use Yii;

class BuildingController extends \yii\rest\ActiveController
{
	use RequestParamsTrait;

	/**
	 * @var string
	 */
	public $modelClass = 'app\models\Building';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['class'] = HttpBearerAuth::className();
		$behaviors['authenticator']['except'] = ['index', 'view'];

		return $behaviors;
	}

	/**
	 * @param string $action
	 * @param Building|null   $model
	 * @param array  $params
	 *
	 * @throws ForbiddenHttpException
	 */
	public function checkAccess($action, $model = null, $params = [])
	{
		switch ($action) {
			case 'update':
			case 'delete':
			case 'imgeadd':
				if ($model->owner != \Yii::$app->user->getId()) {
					throw new ForbiddenHttpException('access error');
				}
				break;
		}
	}

	public function actionImageadd($id)
	{
		$image = Yii::$app->services->filemanager->saveTmpImage('buildings/'.$id,  $_FILES['image']);
		return Building::addImage($id, $image);
	}

	public function actionImagedel($id)
	{
		$requestParams = $this->getRequestParams();

		$url = $requestParams['url'];
		 Yii::$app->services->filemanager->deleteByUrl($url);

		return Building::delImage($id, $url);
	}

}
