<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\User;
use app\modules\api\modules\v1\components\traits\RequestParamsTrait;
use app\modules\api\modules\v1\components\user\forms\ApiResponseForm;
use app\modules\api\modules\v1\components\user\forms\PasswordResetForm;
use app\modules\api\modules\v1\components\user\forms\PasswordRestoreForm;
use yii\filters\auth\HttpBearerAuth;
use Yii;


class UserController extends \yii\rest\ActiveController
{
	use RequestParamsTrait;

	public $modelClass = 'app\models\User';

	public function behaviors()
	{
		$behaviors = parent::behaviors();

		unset($behaviors['authenticator']);
		$behaviors['corsFilter']=[
			'class'=>\yii\filters\Cors::className(),
			'cors'=>[
				"Origin"=>['*'],
				'Access-Control-Request-Method' => ['POST', 'GET','PUT', 'OPTIONS'],
				// Allow only POST and PUT methods
				'Access-Control-Request-Headers' => ['*'],
				// Allow only headers 'X-Wsse'
				// 'Access-Control-Allow-Credentials' => true,
				// Allow OPTIONS caching
				'Access-Control-Max-Age' => 3600,
				// Allow the X-Pagination-Current-Page header to be exposed to the browser.
				'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
			]
		];
		$behaviors['authenticator']['class'] = HttpBearerAuth::className();
		$behaviors['authenticator']['except'] = ['create', 'options', 'restore','resetpwd'];

		return $behaviors;
	}

	public function actions()
	{
		$actions = parent::actions();
		unset($actions['index']);
		return $actions;
	}

	public function actionRestore(){

		$requestParams = $this->getRequestParams();

		$form = PasswordRestoreForm::make($requestParams);

		$form->validateOrException();

		Yii::$app->services->pwdRestore->create($form->email);

		return \GuzzleHttp\json_encode(['status'=>'ok']);

	}

	public function actionResetpwd(){
		$requestParams = $this->getRequestParams();

		$form = PasswordResetForm::make($requestParams);

		$form->validateOrException();

		Yii::$app->services->pwdRestore->reset($form->token, $form->password);

		return \GuzzleHttp\json_encode(['status'=>'ok']);

	}

	public function actionIndex(){

		$user =  User::findOne(["id"=>Yii::$app->user->getId()]);
		return ApiResponseForm::make( $user->getAttributes() );

	}

}
